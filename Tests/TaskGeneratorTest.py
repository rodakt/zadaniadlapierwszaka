import unittest
from TaskGenerator import TaskGenerator


class TaskGeneratorTest(unittest.TestCase):
    def test_adding(self):
        for i in range(1000):
            generator = TaskGenerator('+', 1000)
            a, b, c = generator.get_task()
            self.assertEqual(a + b, c)

    def test_limit(self):
        for i in range(1000):
            generator = TaskGenerator('+', i)
            a, b, c = generator.get_task()
            self.assertTrue(c <= i)


if __name__ == '__main__':
    unittest.main()
