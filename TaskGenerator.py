import random


class TaskGenerator:
    def __init__(self, operator, result_limit):
        self._operator = operator
        self._result_limit = result_limit
        self.task_generator = {
            '+': self._addition_task
        }

    def _addition_task(self):
        first_number = random.randint(0, self._result_limit)
        second_number = random.randint(0, self._result_limit - first_number)
        result = first_number + second_number
        return first_number, second_number, result

    def get_task(self):
        return self.task_generator[self._operator]()

